package com.shipra.ooad;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author shipra
 */
public class Chef implements Employee {
    /** unique chefId */
    private int chefId;
    BroncoDiners diners;
    Lock chefOrderQueueLock, chefDoneQueueLock;

    public Chef(BroncoDiners diners,int chefId) {
        this.diners = diners;
        this.chefId = chefId;
        chefOrderQueueLock = new ReentrantLock();
        chefDoneQueueLock = new ReentrantLock();
    }

    public void run() {
        System.out.println(Thread.currentThread().getName() + " Chef#" + chefId + " started");
        while (true) {
        	try {
        		// Take 2 seconds before picking another order.
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
            chefOrderQueueLock.lock();
            if (!diners.chefOrderQueue.isEmpty()) {
                Customer customerOrder = diners.chefOrderQueue.poll();
                StringBuilder sb = new StringBuilder();
                for (Customer cust : diners.chefOrderQueue) {
                	sb.append(cust.getCustomerId()).append(" ");
                }
                diners.gui.foodToPrepareQueue.setText(sb.toString());

                chefOrderQueueLock.unlock();
                System.out.println("Chef#" + this.chefId + " preparing order of Customer#" + customerOrder.getCustomerId());
                try {
                    diners.chefDoneQueueLock.lock();
                    diners.chefDoneQueue.add(customerOrder);

                    sb = new StringBuilder();
                    for (Customer cust : diners.chefDoneQueue) {
                    	sb.append(cust.getCustomerId()).append(" ");
                    }
                    diners.gui.foodCookedQueue.setText(sb.toString());

                    System.out.println("Chef#" + this.chefId + " prepared order of Customer#" + customerOrder.getCustomerId() + " and placed in chefDoneQueue.");

                	// Take 2 seconds to prepare the order.
                    Thread.sleep(2000);
                    diners.chefDoneQueueLock.unlock();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            } else {
                chefOrderQueueLock.unlock();
            }
        }
    }

    /**
     * @return chefId
     */
    public int getId() {
        return chefId;
    }

    public void setId(int chefId) {
        this.chefId = chefId;
    }

}
