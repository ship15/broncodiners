package com.shipra.ooad;

import javax.swing.JLabel;

import com.shipra.ooad.Customer;
import com.shipra.ooad.gui.ShowGUI;

/**
 * Tracks each table's state
 * 
 * @author shipra
 */
@SuppressWarnings("serial")
public class Table extends JLabel {
	private int tableId;
	private int capacity;
	private boolean free;
	private Customer customer;

	public Table(int tableId, int capacity, boolean free, Customer customer) {
		this.setIcon(ShowGUI.GREEN_TABLE_COLOR);
		this.tableId = tableId;
		this.capacity = capacity;
		this.free = true;
		this.customer = customer; 
		this.setText("<HTML> TableSize#" + capacity + "<br> Status: Table Is Free</HTML>");
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public void setFree(boolean free) {
		this.free = free;
		this.setText("<HTML> TableSize#" + capacity + "<br> Status: Table Is Free</HTML>");
		this.setIcon(ShowGUI.GREEN_TABLE_COLOR);
	}

	public void customerWaitingForServer() {
		this.setText("<HTML> TableSize#" + capacity + "<br> Status: CustomerWaitingForServer</HTML>");
		this.setIcon(ShowGUI.YELLOW_TABLE_COLOR);
	}

	public void customerWaitingForFood() {
		this.setText("<HTML> TableSize#" + capacity + "<br> Status: CustomerWaitingForFood</HTML>");
		this.setIcon(ShowGUI.ORANGE_TABLE_COLOR);
	}

	public void customerEatingFood() {
		this.setText("<HTML> TableSize#" + capacity + "<br> Status: CustomerEatingFood</HTML>");
		this.setIcon(ShowGUI.PURPLE_TABLE_COLOR);
	}

	public void customerWaitingForCheck() {
		this.setText("<HTML> TableSize#" + capacity + "<br> Status: CustomerWaitingForCheck</HTML>");
		this.setIcon(ShowGUI.RED_TABLE_COLOR);
	}

	public int getTableId() {
		return tableId;
	}

	public int getCapacity() {
		return capacity;
	}

	public boolean isFree() {
		return free;
	}
}
