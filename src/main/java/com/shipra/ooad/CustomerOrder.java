package com.shipra.ooad;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * This class generates and manages each customer's order and the order bill.
 * @author shipra
 */
public class CustomerOrder {
    private double billAmount;
    private Customer customer;
    private int menuSize;
    private Map<Integer, Menu> orderHelp;

    public CustomerOrder(Customer customer) {
        this.customer = customer;
        orderHelp = new HashMap<>();
        menuSize = Menu.getMenuSize();
        orderHelp.put(1, Menu.Burger);
        orderHelp.put(2, Menu.Pizza);
        orderHelp.put(3, Menu.VeggieWrap);
        orderHelp.put(4, Menu.VeggieRoll);
        orderHelp.put(5, Menu.FullMeal);
        orderHelp.put(6, Menu.Salad);
        orderHelp.put(7, Menu.Soup);
        orderHelp.put(8, Menu.Soda);
        orderHelp.put(9, Menu.Coffee);
        orderHelp.put(10, Menu.Tea);
        this.billAmount = orderBill();
    }
    
    /**
     * Generate customer food order and billing.
     * @return orderBill
     */
    public double orderBill() {
        double amount = 0;
        int partySize = customer.getPartySize();
        int numberOfItems = 0;

        if (partySize <= 2) {
            numberOfItems = 3;
        } else if (partySize <= 4) {
            numberOfItems = 5;
        }
        int firstItemNum = 1;
        int lastItemNum = menuSize;
        int dif = lastItemNum - firstItemNum;
        for (int counter = 1; counter <= numberOfItems; counter++) {
            Random randomNum = new Random();
            int itemNumber = randomNum.nextInt(dif) + firstItemNum;
            amount += orderHelp.get(itemNumber).getItemPrice(itemNumber);
        }
        return amount;
    }

    /**
     * @return billAmount
     */
    public double getBillAmount() {
        return billAmount;
    }   
}
