package com.shipra.ooad;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class is for each Customer who dines in and performs following tasks:
 * Customer and Server interaction -- Customer orders food to server; 
 * eats food when served by the server and requests check from the server. 
 * @author shipra
 */
public class Customer implements Runnable {
    private int customerId;
    private int partySize;
    private Table table;
    private int serverId;
    CustomerStatus status;
    private CustomerOrder orderBill;
    BroncoDiners diners;
    /** Lock on customer state */
    Lock customerLock;
    /** Condition variable on customer lock. */
    Condition customerCv;

    public Customer(BroncoDiners diners, int customerId, int partySize) {
        this.diners = diners;
    	this.customerId = customerId;
    	this.partySize = partySize;
        customerLock = new ReentrantLock();
        customerCv = customerLock.newCondition();
    }

    @Override
    public void run() {
    	System.out.println(Thread.currentThread().getName() + " customer started.");
        diners.orderPlaceQueueLock.lock();
        customerLock.lock();
        diners.getOrderPlaceQueue().add(this);
        diners.orderPlaceQueueLock.unlock();
        try {
            table.customerWaitingForServer();
            System.out.println("Customer#" + this.customerId + " waiting for server");
            status = CustomerStatus.WAITING_FOR_SERVER;
            customerCv.await();

            Thread.sleep(2000);
            this.orderBill = new CustomerOrder(this);
            table.customerWaitingForFood();
            System.out.println("Customer#" + this.customerId + " Ordered to server#" + serverId + " Waiting for food...");
            status = CustomerStatus.ORDER_PLACED;
            customerCv.await();

            table.customerEatingFood();
            System.out.println("Customer#" + customerId + " received food. Eating...");
            status = CustomerStatus.EATING_FOOD;
            Thread.sleep(5000);

            table.customerWaitingForCheck();
            System.out.println("Customer#" + customerId + " requesting check to server#" + serverId);
            status = CustomerStatus.WAITING_BILL;
            customerCv.await();

            System.out.println("Customer#" + customerId + " received check/bill.");
            status = CustomerStatus.CHECKOUT;
            Thread.sleep(2000);
            customerCv.signal();
            customerCv.await();
            customerLock.unlock();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Customer#" + customerId + " leaving restaurant. ");
    }

    /**
     * @return customer orderBill
     */
    public CustomerOrder getOrderBill() {
        return orderBill;
    }

    /**
     * @return customer status. Status changes when customer state changes.
     */
    public CustomerStatus getStatus() {
        return status;
    }

    /**
     * Set table when customer is assigned a table  
     * @param table
     */
    public void setTable(Table table) {
        this.table = table;
    }

    /**
     * Set serverId when server is assigned to customer
     * @param serverId
     */
    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public int getPartySize() {
        return partySize;
    }

    public Table getTable() {
        return table;
    }
}
