package com.shipra.ooad.gui;

import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

import com.shipra.ooad.Table;

@SuppressWarnings("serial")
public class TablePanel extends JPanel implements MouseListener {
	public Table[] tables = new Table[6];

	public TablePanel(){
		tables[0] = new Table(0, 2, true, null);
		tables[1] = new Table(1, 2, true, null);
		tables[2] = new Table(2, 2, true, null);
		tables[3] = new Table(3, 4, true, null);
		tables[4] = new Table(4, 4, true, null);
		tables[5] = new Table(5, 4, true, null);

		this.setLayout(new GridLayout(2, 3, 10, 5));
		
		for(int i =0; i<tables.length; i++) {
			this.add(tables[i]);
			tables[i].addMouseListener(this);
		}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
	}
}
