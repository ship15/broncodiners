package com.shipra.ooad.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.shipra.ooad.BroncoDiners;
import com.shipra.ooad.Customer;

/**
 * Created by yaolan on 3/4/17.
 */
public class ShowGUI {
	private JFrame mainFrame;

	// the 4 basic panel
	private JPanel topPanel;
	private JPanel rightPanel;
	private JPanel bottomPanel;
	public TablePanel tablePanel;

	// sub panels of topPanel
	private JPanel chefPanel;
	public JLabel foodCookedQueue;
	public JLabel foodToPrepareQueue;

	private JPanel titlePanel;
	// Server panel
	private JPanel serverPanel;
	public JLabel[] serverLabel = new JLabel[2];

	// sub panels of right panel
	private JPanel waitingQueuePanel;
	private JPanel addToWaitingPanel;

	// sub panels of bottom panel
	private JPanel totalEarningPanel;
	private JPanel checkoutPanel;
	public JLabel totalEarning;

	// Font Constants
	private static final Font SUB_TITLE_FONT = new Font("SansSerif", Font.PLAIN, 18);
	private static final Font LABEL_FONT = new Font("SansSerif", Font.PLAIN, 16);
	private static final Font TITLE_FONT = new Font("SansSerif", Font.BOLD, 24);

	// Dimension Constants of panels
	private static final Dimension FRAME_SIZE = new Dimension(1200, 750);
	private static final Dimension TOP_PANEL_SIZE = new Dimension(800, 200);
	private static final Dimension SIDE_PANEL_SIZE = new Dimension(200, 400);
	private static final Dimension CHEF_SERVER_SIZE = new Dimension(400, 350);
	private static final Dimension ADD_TO_PANEL_SIZE = new Dimension(200, 100);
	private static final Dimension BOTTOM_PANEL_SIZE = new Dimension(800, 150);
	private static final Dimension WAITING_LIST_SIZE = new Dimension(200, 260);
	private static final Dimension TOTAL_EARNING_SIZE = new Dimension(200, 150);
	private static final Dimension SCROLL_PAN_SIZE = new Dimension(180, 150);
	private static final Dimension TITLE_PANEL_SIZE = new Dimension(800, 50);
	private static final Dimension CHECKOUT_PANEL_SIZE = new Dimension(200, 150);

	// Color Constants
	private static final Color BORDER_COLOR = new Color(199, 0, 57);
	private static final Color PANEL_BACKGROUND_COLOR = new Color(255, 204, 188);
	private static final Color PANEL_BORDER_COLOR = Color.GRAY;
	private static final Color TITLE_COLOR = new Color(144, 12, 63);

	// Name Constants
	private static final String CHEF_VIEW = "Chef View";
	private static final String SERVER_VIEW = "Server View";
	private static final String TABLE_VIEW = "Table View";
	private static final String WAITING_LIST = "Waiting List";
	private static final String MANAGER_VIEW = "Manager View";
	private static final String TITLE = "Bronco Diners";

	public static final Icon GREEN_TABLE_COLOR = new ImageIcon("light green circle.png");
	public static final Icon RED_TABLE_COLOR = new ImageIcon("light red circle.png");
	public static final Icon BLUE_TABLE_COLOR = new ImageIcon("light blue circle.png");
	public static final Icon ORANGE_TABLE_COLOR = new ImageIcon("light orange circle.png");
	public static final Icon PURPLE_TABLE_COLOR = new ImageIcon("light purple circle.png");
	public static final Icon YELLOW_TABLE_COLOR = new ImageIcon("light yellow circle.png");

	// waiting panel components
	int customerId = 1;
	public Queue<Customer> customerWaitQueue = new LinkedList<>();
	public Lock customerWaitQueueLock = new ReentrantLock();

	// addToWaitingPanel
	private JButton addToWaitingListButton;
	public JScrollPane scrollPane;
	JTextField sizeInput;

	// checkoutPanel components
	private JLabel customerWaitingCheckoutLabel;
	public JLabel customerIdWaitingToCheckout;

	public ShowGUI() {
		prepareGUI();
		mainFrame.setVisible(true);

		addToWaitingListButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String newWaiting = sizeInput.getText();
				Customer customer = new Customer(null, customerId++, Integer.parseInt(newWaiting));
				customerWaitQueue.add(customer);
				scrollPane.setViewportView(setWaitingJList());
				sizeInput.setText("");
			}
		});
	}

	// prepare the 4 basic panels(top, center(tablePanel), right, bottom)
	private void prepareGUI() {
		mainFrame = new JFrame("Restaurant System");
		mainFrame.setSize(FRAME_SIZE);
		mainFrame.setBackground(Color.black);
		mainFrame.setLayout(new BorderLayout(2, 2));

		// init the basic 4 panels with the size and background color
		topPanel = new JPanel();
		topPanel.setPreferredSize(TOP_PANEL_SIZE);

		bottomPanel = new JPanel();
		bottomPanel.setPreferredSize(BOTTOM_PANEL_SIZE);

		rightPanel = new JPanel();
		tablePanel = new TablePanel();

		rightPanel.setPreferredSize(SIDE_PANEL_SIZE);

		tablePanel.setBorder(BorderFactory.createLineBorder(BORDER_COLOR));

		mainFrame.add(topPanel, BorderLayout.NORTH);
		mainFrame.add(tablePanel, BorderLayout.CENTER);
		mainFrame.add(bottomPanel, BorderLayout.SOUTH);
		mainFrame.add(rightPanel, BorderLayout.EAST);

		setTopPanel();
		setRightPanel();
		setBottomPanel();
		// setPanelBorderColor(BORDER_COLOR);
		setPanelBorderTitledBorder();

		setPanelBackgroundColor(PANEL_BACKGROUND_COLOR);
	}

	@SuppressWarnings("unused")
	private void setPanelBorderColor(Color color) {
		chefPanel.setBorder(BorderFactory.createLineBorder(color));
		serverPanel.setBorder(BorderFactory.createLineBorder(color));
		tablePanel.setBorder(BorderFactory.createLineBorder(color));
		rightPanel.setBorder(BorderFactory.createLineBorder(color));
		bottomPanel.setBorder(BorderFactory.createLineBorder(color));
	}

	// set background for every sub panels
	private void setPanelBackgroundColor(Color color) {

	}

	// set borders with titles for each panel
	private void setPanelBorderTitledBorder() {
		TitledBorder chefPanelBorder = BorderFactory.createTitledBorder(null, CHEF_VIEW,
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, SUB_TITLE_FONT, BORDER_COLOR);
		TitledBorder serverPanelBorder = BorderFactory.createTitledBorder(null, SERVER_VIEW,
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, SUB_TITLE_FONT, BORDER_COLOR);
		TitledBorder tablePanelBorder = BorderFactory.createTitledBorder(null, TABLE_VIEW,
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, SUB_TITLE_FONT, BORDER_COLOR);
		TitledBorder rightPanelBorder = BorderFactory.createTitledBorder(null, WAITING_LIST,
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, SUB_TITLE_FONT, BORDER_COLOR);
		TitledBorder bottomPanelBorder = BorderFactory.createTitledBorder(null, MANAGER_VIEW,
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, SUB_TITLE_FONT, BORDER_COLOR);

		chefPanel.setBorder(chefPanelBorder);
		serverPanel.setBorder(serverPanelBorder);
		tablePanel.setBorder(tablePanelBorder);
		rightPanel.setBorder(rightPanelBorder);
		bottomPanel.setBorder(bottomPanelBorder);
	}

	private void setTitlePanel() {
		titlePanel = new JPanel();
		titlePanel.setPreferredSize(TITLE_PANEL_SIZE);
		titlePanel.setBackground(TITLE_COLOR);
		JLabel title = new JLabel(TITLE);
		title.setFont(TITLE_FONT);
		titlePanel.add(title);
	}

	private void setTopPanel() {
		// init the 3 sub panels of topPanel
		setChefPanel();
		setServerPanel();

		// manage topPanel, add 3 sub panels to it
		topPanel.setLayout(new BorderLayout(2, 2));
		topPanel.add(chefPanel, BorderLayout.WEST);
		topPanel.add(serverPanel, BorderLayout.CENTER);

		setTitlePanel();
		topPanel.add(titlePanel, BorderLayout.NORTH);
	}

	private void setRightPanel() {
		rightPanel.setLayout(new BorderLayout());
		setWaitingListPanel();
		setAddToWaitingPanel();
		rightPanel.add(waitingQueuePanel, BorderLayout.NORTH);
		rightPanel.add(addToWaitingPanel, BorderLayout.CENTER);
	}

	private void setBottomPanel() {
		bottomPanel.setLayout(new BorderLayout());

		// For displaying the checkout queue
		setCheckoutPanel();

		// For displaying total earnings.
		setTotalEarningPanel();

		bottomPanel.add(checkoutPanel, BorderLayout.WEST);
		bottomPanel.add(totalEarningPanel, BorderLayout.EAST);

	}

	// return a JList with waitingList data
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public JList<String> setWaitingJList() {
		List<String> list = new ArrayList<>();
		for (Customer customer : customerWaitQueue) {
			list.add(customer.getPartySize() + "people");
		}
		return new JList(list.toArray());
	}

	private void setWaitingListPanel() {
		waitingQueuePanel = new JPanel();
		waitingQueuePanel.setPreferredSize(WAITING_LIST_SIZE);

		JList<String> list = setWaitingJList();
		scrollPane = new JScrollPane();
		scrollPane.setViewportView(list);
		scrollPane.setPreferredSize(SCROLL_PAN_SIZE);

		JLabel tileOfWaiting = new JLabel("");
		// tileOfWaiting.setFont(TITLE_FONT);
		waitingQueuePanel.setLayout(new BorderLayout());
		waitingQueuePanel.add(tileOfWaiting, BorderLayout.NORTH);
		waitingQueuePanel.add(scrollPane, BorderLayout.CENTER);
	}

	private void setAddToWaitingPanel() {
		addToWaitingPanel = new JPanel();
		addToWaitingPanel.setPreferredSize(ADD_TO_PANEL_SIZE);
		JLabel tileOfChief = new JLabel("Party Size: ");
		sizeInput = new JTextField("", 6);
		addToWaitingListButton = new JButton("Add to WaitingList");
		addToWaitingPanel.add(tileOfChief);
		addToWaitingPanel.add(sizeInput);
		addToWaitingPanel.add(addToWaitingListButton);
	}

	private void setServerPanel() {
		serverPanel = new JPanel();
		serverPanel.setPreferredSize(CHEF_SERVER_SIZE);
		serverPanel.setLayout(new BoxLayout(serverPanel, BoxLayout.Y_AXIS));

		JLabel server1 = new JLabel("Customers served by Server#1");
		server1.setFont(SUB_TITLE_FONT);

		JLabel server2 = new JLabel("Customers served by Server#2");
		server2.setFont(SUB_TITLE_FONT);

		serverPanel.add(server1);
		serverLabel[0] = new JLabel("");
		serverLabel[0].setPreferredSize(new Dimension(100, 350));
		serverLabel[0].setFont(SUB_TITLE_FONT);
		serverPanel.add(serverLabel[0]);
		serverPanel.add(server2);
		serverLabel[1] = new JLabel("");
		serverLabel[1].setPreferredSize(new Dimension(100, 350));
		serverLabel[1].setFont(SUB_TITLE_FONT);
		serverPanel.add(serverLabel[1]);
	}

	private void setChefPanel() {
		chefPanel = new JPanel();
		chefPanel.setPreferredSize(CHEF_SERVER_SIZE);
		chefPanel.setLayout(new BoxLayout(chefPanel, BoxLayout.Y_AXIS));

		JPanel pendingOrderPanel = new JPanel();
		pendingOrderPanel.setPreferredSize(new Dimension(100, 350));
		pendingOrderPanel.setLayout(new BoxLayout(pendingOrderPanel, BoxLayout.Y_AXIS));
		chefPanel.add(pendingOrderPanel);

		JPanel prepardOrderPanel = new JPanel();
		prepardOrderPanel.setPreferredSize(new Dimension(100, 350));
		prepardOrderPanel.setLayout(new BoxLayout(prepardOrderPanel, BoxLayout.Y_AXIS));
		chefPanel.add(prepardOrderPanel);

		JLabel pendingOrder = new JLabel("Orders Chef needs to prepare");
		pendingOrder.setFont(SUB_TITLE_FONT);
		pendingOrder.setHorizontalTextPosition(2);
		JLabel preparedOrder = new JLabel("Orders Chef has prepared");
		preparedOrder.setFont(SUB_TITLE_FONT);
		preparedOrder.setHorizontalTextPosition(2);

		pendingOrderPanel.add(pendingOrder);
		foodToPrepareQueue = new JLabel();
		foodToPrepareQueue.setFont(SUB_TITLE_FONT);
		pendingOrderPanel.add(foodToPrepareQueue);

		prepardOrderPanel.add(preparedOrder);
		foodCookedQueue = new JLabel();
		foodCookedQueue.setFont(SUB_TITLE_FONT);
		prepardOrderPanel.add(foodCookedQueue);
	}

	private void setCheckoutPanel() {
		checkoutPanel = new JPanel();
		customerWaitingCheckoutLabel = new JLabel("Customers waiting for checkout: ");
		customerWaitingCheckoutLabel.setFont(SUB_TITLE_FONT);

		customerIdWaitingToCheckout = new JLabel();
		customerIdWaitingToCheckout.setFont(SUB_TITLE_FONT);

		checkoutPanel.add(customerWaitingCheckoutLabel);
		checkoutPanel.add(customerIdWaitingToCheckout);
	}

	private void setTotalEarningPanel() {
		totalEarningPanel = new JPanel();
		totalEarningPanel.setPreferredSize(TOTAL_EARNING_SIZE);

		JLabel title = new JLabel("Total Earning:");
		title.setFont(SUB_TITLE_FONT);

		totalEarning = new JLabel();
		totalEarning.setFont(LABEL_FONT);

		totalEarningPanel.add(title);
		totalEarningPanel.add(totalEarning);
	}

	public static void main(String[] args) {
		ShowGUI gui = new ShowGUI();
		new BroncoDiners(gui);
	}
}
