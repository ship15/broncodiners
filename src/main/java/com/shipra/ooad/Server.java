package com.shipra.ooad;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This class is for each server and does following tasks:
 * implements server and his customer interaction at various times when customer is dining.
 * @author shipra
 */
public class Server implements Employee {
    private int serverId;
    /** Each server has his customerServing List which stores customers the server is serving at any particular point of time.*/
    private List<Customer> customerServing = new ArrayList<>();
    private BroncoDiners diners;

    public Server(BroncoDiners diners,int serverId) {
        this.diners = diners;
        this.serverId = serverId;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " Server#" + this.serverId + " started.");
        while (true) {
        	try {
        		// First get customer from common Queue to it's local queue.
                diners.orderPlaceQueueLock.lock();
                if (!diners.getOrderPlaceQueue().isEmpty()) {
                    Customer customer = diners.getOrderPlaceQueue().poll();
                    customer.setServerId(this.serverId);
                    customerServing.add(customer);
                    diners.orderPlaceQueueLock.unlock();
                } else {
                	diners.orderPlaceQueueLock.unlock();
                }

                StringBuilder sb = new StringBuilder();
                for (Customer cust : customerServing) {
                	sb.append(cust.getCustomerId()).append(" ");
                }
                diners.gui.serverLabel[serverId-1].setText(sb.toString());

                // Yield cpu
                Thread.sleep(500);

                // Now go over local queue and see if any customer is waiting for anything.
                List<Customer> processedList = new ArrayList<>();
                Iterator<Customer> itr = customerServing.iterator();
                while(itr.hasNext()) {
                	Customer customer = itr.next();
                	itr.remove();
                	if(customer.customerLock.tryLock()) {
                    	switch(customer.getStatus()) {
                    	case WAITING_FOR_SERVER:
                            System.out.println("Customer#" + customer.getCustomerId() + " is assigned server#" + this.serverId);
                            System.out.println("Server#" + this.serverId + " is taking Customer#" + customer.getCustomerId() + " order");
                            customer.customerCv.signal();
                        	processedList.add(customer);
                    		break;
                    	case ORDER_PLACED:
                    		// Customer has decided it's order - Now take the order & give it to the chef
                            diners.chefOrderQueueLock.lock();
                            diners.chefOrderQueue.add(customer);
                            customer.status = CustomerStatus.WAITING_FOR_FOOD;

                            // Update the Chef GUI panel
                            sb = new StringBuilder();
                            for (Customer cust : diners.chefOrderQueue) {
                            	sb.append(cust.getCustomerId()).append(" ");
                            }
                            diners.gui.foodToPrepareQueue.setText(sb.toString());

                            System.out.println("Server#" + this.serverId + " placed order of Customer#" + customer.getCustomerId() + " in chef order queue.");
                            diners.chefOrderQueueLock.unlock();
                        	processedList.add(customer);
                    		break;
                    	case WAITING_FOR_FOOD:
                    		// System.out.println("In WAITING_FOR_FOOD");
                    		diners.chefDoneQueueLock.lock();
                    		if(diners.chefDoneQueue.contains(customer)) {
                    			diners.chefDoneQueue.remove(customer);

                    			// Update Chef's Done queue in GUI
                                sb = new StringBuilder();
                                for (Customer cust : diners.chefDoneQueue) {
                                	sb.append(cust.getCustomerId()).append(" ");
                                }
                                diners.gui.foodCookedQueue.setText(sb.toString());

                                System.out.println("Server#" + serverId + " serving food to Customer#" + customer.getCustomerId());
                                customer.customerCv.signal();
                    		}
                        	diners.chefDoneQueueLock.unlock();
                        	processedList.add(customer);
                    		break;
                    	case EATING_FOOD:
                        	processedList.add(customer);
                    		break;
                    	case WAITING_BILL:
                            diners.checkoutQueueLock.lock();
                            diners.checkoutQueue.add(customer);

                            sb = new StringBuilder();
                            for (Customer cust : diners.checkoutQueue) {
                            	sb.append(cust.getCustomerId()).append(" ");
                            }
                            diners.gui.customerIdWaitingToCheckout.setText(sb.toString());

                            System.out.println("Server#" + serverId + " added Customer#" + customer.getCustomerId() + " to checkout Queue.");
                            diners.checkoutQueueLock.unlock();
                            break;
                    	case CHECKOUT:
                    		break;
                    	}
                        customer.customerLock.unlock();
                	} else {
                		processedList.add(customer);
                	}
                }
                customerServing.addAll(processedList);
        	} catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * @return serverId
     */
    public int getId() {
        return serverId;
    }
}
