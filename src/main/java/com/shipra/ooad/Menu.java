package com.shipra.ooad;

/**
 * This enum describes the menu of the Bronco Diners restaurant.
 * @author shipra
 */
public enum Menu {
	Burger(1,7.99),
	Pizza(2,10.99),
	VeggieWrap(3,6.99),
	VeggieRoll(4,7.99),
	FullMeal(5,11.99),
	Salad(6,8.99),
	Soup(7,5.99),
	Soda(8,2.99),
	Coffee(9,3.99),
	Tea(10,3.55); 
	
	private int itemNum;
	private double itemPrice;
	private static int menuSize = 10;
	
    private Menu(int itemNum, double itemPrice){
        this.itemNum = itemNum;
        this.itemPrice = itemPrice;        
    }

	public static int getMenuSize() {
        return menuSize;
    }

    public int getItemNum() {
        return itemNum;
    }

    public double getItemPrice(int itemNumber) {
        return itemPrice;
    }
}
