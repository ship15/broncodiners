package com.shipra.ooad;

/**
 * Manager is responsible for assigning seat to the customer and managing checkout of the customer out of restaurant.
 * And also manages total earnings of the day.
 * @author shipra
 */
public class Manager implements Employee {
    private int managerId;
    private TablePool tablePool;
    private BroncoDiners diners;
    private double totalEarnings;

    public Manager(BroncoDiners diners,int managerId) {
        this.managerId = managerId;
        this.tablePool = diners.getTablePool();
        this.diners = diners;
        totalEarnings = diners.getTotalEarningsOfDay();
    }

    @Override
    public void run() {
    	System.out.println(Thread.currentThread().getName() + " manager is started.");
        while (true) {
        	boolean noTableFound = false;
        	while (!diners.gui.customerWaitQueue.isEmpty() && !tablePool.freeTablePool.isEmpty()) {
                diners.gui.customerWaitQueueLock.lock();
                Customer customer = diners.gui.customerWaitQueue.peek();
                customer.diners = diners;
                int partySize = customer.getPartySize();
                Table table = getTable(partySize);
                if (table != null) {
                    customer = diners.gui.customerWaitQueue.poll();
                    customer.setTable(table);
                    table.setCustomer(customer);
                    
                    diners.gui.scrollPane.setViewportView(diners.gui.setWaitingJList());

                    // assign server to customer
                    System.out.println("Customer#" + customer.getCustomerId() + " is assigned table#" + table.getTableId());
                    tablePool.removeFreeTablePool(table);
                    tablePool.addOccupiedTable(table);
                    Thread customerThread = new Thread(customer);
                    customerThread.start();
                    diners.gui.customerWaitQueueLock.unlock();
                } else if (partySize == 4) {
                	System.out.println("Table not found for partySize - " + partySize);
                	// Try to see if table is there for size of two and there is a part of size <=2.
                    Table priorityTable = getTable(2);
                    Customer priorityCustomer = getCustomer(2);
                    if(priorityTable == null || priorityCustomer == null) {
                    	System.out.println("No Table found for partySize - 2");
                    	diners.gui.customerWaitQueueLock.unlock();
                    	noTableFound = true;
                    } else {
                    	System.out.println("Table found for partySize - 2");
                        priorityCustomer.diners = this.diners;
                    	diners.gui.customerWaitQueue.remove(priorityCustomer);
                        priorityCustomer.setTable(priorityTable);
                        priorityTable.setCustomer(priorityCustomer);

                        diners.gui.scrollPane.setViewportView(diners.gui.setWaitingJList());

                        System.out.println("Customer#" + priorityCustomer.getCustomerId() + " is assigned table#" + priorityTable.getTableId());
                        tablePool.removeFreeTablePool(priorityTable);
                        tablePool.addOccupiedTable(priorityTable);
                        Thread priorityCustomerThread = new Thread(priorityCustomer);
                        priorityCustomerThread.start();
                        diners.gui.customerWaitQueueLock.unlock();
                    }
                } else {
                	diners.gui.customerWaitQueueLock.unlock();
                	break;
                }
                if(noTableFound) {
                    try {
        				Thread.sleep(2000);
        			} catch (InterruptedException e) {
        				e.printStackTrace();
        			}
                	break;
                }
        	}

            try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

            diners.checkoutQueueLock.lock();
            if (!diners.checkoutQueue.isEmpty()) {
                Customer customer = diners.checkoutQueue.poll();

                StringBuilder sb = new StringBuilder();
                for (Customer cust : diners.checkoutQueue) {
                	sb.append(cust.getCustomerId()).append(" ");
                }
                diners.gui.customerIdWaitingToCheckout.setText(sb.toString());
                diners.checkoutQueueLock.unlock();

                customer.customerLock.lock();
                System.out.println("Manager to Customer#" + customer.getCustomerId() + " here is your bill sir.");
                customer.customerCv.signal();
                try {
                    customer.customerCv.await();
                    totalEarnings = totalEarnings + customer.getOrderBill().getBillAmount();
                    diners.setTotalEarningsOfDay(totalEarnings);
                    System.out.println("Customer#" + customer.getCustomerId() + " paid bill amount " + customer.getOrderBill().getBillAmount() + " and exits restaurant.");
                    System.out.println("Total earnings after Customer#" + customer.getCustomerId() + " left are " + totalEarnings);
                    Table table = customer.getTable();
                    tablePool.removeOccupiedTable(table);
                    tablePool.addFreeTablePool(table);
                    customer.customerCv.signal();
                    customer.customerLock.unlock();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("");
            } else {
            	diners.checkoutQueueLock.unlock();
            	// System.out.println("Checkout queue empty.");
            }
        }
    }
    

    /**
     * @param partySize
     * @return table of capacity greater than or equal to partySize from freeTablePool if available 
     *         else return null
     */
    private Table getTable(int partySize) {
        for (Table table : tablePool.freeTablePool) {
            int capacity = table.getCapacity();
            if (partySize <= capacity) {
                return table;
            }
        }
        return null;
    }
    

    /**
     * 
     * @param partySize
     * @return customer of partySize 2 or less in case of prioritization. 
     */
    private Customer getCustomer(int partySize) {
        for (Customer customer : diners.gui.customerWaitQueue) {
            int customerPartySize = customer.getPartySize();
            if (customerPartySize <= partySize) {
                return customer;
            }
        }
        return null;
    }

    /**
     * @return managerId
     */
    public int getId() {
        return managerId;
    }
}