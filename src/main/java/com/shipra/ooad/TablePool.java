package com.shipra.ooad;

import java.util.ArrayList;
import java.util.List;

/**
 * Tracks which tables are free and which ones are occupied.
 * 
 * @author shipra
 */
public class TablePool {
	List<Table> freeTablePool;
	List<Table> occupiedTable;
	int freeTablePoolSize;
	int occupiedTablePoolSize;

	public TablePool() {
		freeTablePool = new ArrayList<>();
		occupiedTable = new ArrayList<>();
		freeTablePoolSize = 0;
		occupiedTablePoolSize = 0;
	}

	public void addFreeTablePool(Table table) {
		freeTablePool.add(table);
		freeTablePoolSize++;
		if (occupiedTablePoolSize > 0) {
			occupiedTablePoolSize--;
		}
	}

	public void removeFreeTablePool(Table table) {
		freeTablePool.remove(table);
		freeTablePoolSize--;
		occupiedTablePoolSize++;

	}

	public void addOccupiedTable(Table table) {
		occupiedTable.add(table);
		occupiedTablePoolSize++;
		freeTablePoolSize--;
	}

	public void removeOccupiedTable(Table table) {
		occupiedTable.remove(table);
		table.setFree(true);
		occupiedTablePoolSize--;
		freeTablePoolSize++;
		;
	}

	public int getFreeTablePoolSize() {
		return freeTablePoolSize;
	}

	public int getOccupiedTablePoolSize() {
		return occupiedTablePoolSize;
	}

}
