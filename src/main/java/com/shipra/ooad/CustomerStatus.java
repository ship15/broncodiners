package com.shipra.ooad;

import java.awt.Color;

/**
 * This enum describes various states of Customer during the time he dines in.
 * @author shipra
 */
public enum CustomerStatus {
    WAITING_FOR_SERVER(Color.CYAN),
    ORDER_PLACED(Color.RED),
    WAITING_FOR_FOOD(Color.RED),
    EATING_FOOD(Color.GREEN),
    WAITING_BILL(Color.BLUE),
    CHECKOUT(Color.yellow);
    
    private CustomerStatus(Color stateColor){
        
    }

}
