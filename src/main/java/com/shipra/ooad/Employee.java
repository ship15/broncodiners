package com.shipra.ooad;

/**
 * This interface is implemented by all employees of the restaurant.
 * @author shipra
 */
public interface Employee extends Runnable {
    /**
     * @return the employee id
     */
    int getId();
}
