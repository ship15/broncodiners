package com.shipra.ooad;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.shipra.ooad.gui.ShowGUI;

/**
 * Entry point for BroncoDiners
 * @author shipraPanwar
 * 
 */
public class BroncoDiners {
    /** After getting seat assigned customers in orderPlaceQueue are waiting for server to be assigned. */
	volatile Queue<Customer> orderPlaceQueue = new LinkedList<>();
    /** Customers ready for checkout are placed in checkoutQueue. */
    volatile ConcurrentLinkedQueue<Customer> checkoutQueue = new ConcurrentLinkedQueue<>();
    /** Customer's order placed in chefOrderQueue from which chef will take order and prepare food. */
    ConcurrentLinkedQueue<Customer> chefOrderQueue = new ConcurrentLinkedQueue<>();
    /** After preparing food, chef place customer order in chefDoneQueue. */
    List<Customer> chefDoneQueue = new ArrayList<>();
    
    TablePool tablePool;
    /** These are locks on shared queues, "QueueNameLock" */
    Lock orderPlaceQueueLock, checkoutQueueLock;
    Lock chefOrderQueueLock, chefDoneQueueLock;
    /** Tracks total earnings of the day */
    private double totalEarningsOfDay;
    ShowGUI gui;
    int customerId = 1;

    public BroncoDiners(ShowGUI gui) {
    	this.gui = gui;

        totalEarningsOfDay = 0;
        tablePool = new TablePool();
        // customerWaitQueueLock = new ReentrantLock();
        orderPlaceQueueLock = new ReentrantLock();
        chefOrderQueueLock = new ReentrantLock();
        chefDoneQueueLock = new ReentrantLock();
        checkoutQueueLock = new ReentrantLock();

        for(Table tableItr : this.gui.tablePanel.tables){
            tablePool.addFreeTablePool(tableItr);
        }
        
        Manager manager = new Manager(this, 1);
        Thread managerThread = new Thread(manager);
        managerThread.start();
        System.out.println("Manager starts ");
        
        Server[] server = new Server[2];
        server[0] = new Server(this, 1);
        server[1] = new Server(this, 2);
        
        
        Thread serverThread1 = new Thread(server[0]);
        serverThread1.start();
        System.out.println("Server1 starts");

        Thread serverThread2 = new Thread(server[1]);
        serverThread2.start();
        System.out.println("Server2 starts");

        Chef chef = new Chef(this, 1);
        Thread chefThread = new Thread(chef);
        chefThread.start();

/**
        Customer newCustomer = new Customer(this, customerId++, 4);
        Customer customer2 = new Customer(this, customerId++, 2);
        Customer customer3 = new Customer(this, customerId++, 4);
        Customer customer4 = new Customer(this, customerId++, 2);
        Customer customer5 = new Customer(this, customerId++, 3);
        Customer customer6 = new Customer(this, customerId++, 1);

        customerWaitQueue.add(newCustomer);
        customerWaitQueue.add(customer2);
        customerWaitQueue.add(customer3);
        customerWaitQueue.add(customer4);
        customerWaitQueue.add(customer5);
        customerWaitQueue.add(customer6);
*/
    }
    
    /**
     * @param totalEarningsOfDay
     */
    public void setTotalEarningsOfDay(double totalEarningsOfDay) {
        this.totalEarningsOfDay = totalEarningsOfDay;
        DecimalFormat df = new DecimalFormat();
        df.setMinimumFractionDigits(2);
        this.gui.totalEarning.setText("$" + df.format(this.totalEarningsOfDay));
    }

    /**
     * @return the total earnings of the day
     */
    public double getTotalEarningsOfDay() {
        return totalEarningsOfDay;
    }

    /**
     * @return table pool
     */
    public TablePool getTablePool() {
        return tablePool;
    }

    /**
     * @return orderPlaceQueue
     */
    public Queue<Customer> getOrderPlaceQueue() {
        return orderPlaceQueue;
    }

}
